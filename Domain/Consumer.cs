﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Consumer
    {
        [Key]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string IpAddress { get; set; }
        public DateTime CreationDate { get; set; }
        
        public Pub PubChosen { get; set; }
        public bool Redeemed { get; set; }
    }
}