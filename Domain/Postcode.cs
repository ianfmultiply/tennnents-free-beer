﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Postcode
    {
        [Key]
        [MaxLength(8)]
        public string Name { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        [MaxLength(20)]
        public string Country { get; set; }
        [MaxLength(20)]
        public string Size { get; set; }
    }
}
