﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Pub
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        [MaxLength(8)]
        public string Postcode { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int Inventory { get; set; }
        
        public ICollection<Consumer> Consumers { get; set; }
    }
}