﻿using System.Collections.Generic;
using System.Text.Json;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Persistence.SeedData;

namespace Persistence
{
    public class DataContext : IdentityDbContext<AppUser> 
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Postcode> Postcodes { get; set; }
        public DbSet<Pub> Pubs { get; set; }
        public DbSet<Consumer> Consumers { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Consumer>()
                .HasOne(x => x.PubChosen);

            modelBuilder.Entity<Pub>()
                .HasMany(x => x.Consumers);

            // Populate the postcodes from a resources file
            // var resources = PostcodeResources.Postcodes;
            // var postcodes = JsonSerializer.Deserialize<List<Postcode>>(resources);
            // modelBuilder.Entity<Postcode>().HasData(postcodes);

        }
    }
}