﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class PostcodeDistanceFunctions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"
				CREATE Function [dbo].[CalculateDistance]
					(@Longitude1 Decimal(8,5), 
					@Latitude1   Decimal(8,5),
					@Longitude2  Decimal(8,5),
					@Latitude2   Decimal(8,5))
				Returns Float
				As
				Begin
				Declare @Temp Float

				Set @Temp = sin(@Latitude1/57.2957795130823) * sin(@Latitude2/57.2957795130823) + cos(@Latitude1/57.2957795130823) * cos(@Latitude2/57.2957795130823) * cos(@Longitude2/57.2957795130823 - @Longitude1/57.2957795130823)

				if @Temp > 1 
					Set @Temp = 1
				Else If @Temp < -1
					Set @Temp = -1

				Return (3958.75586574 * acos(@Temp)	) 

				End
				GO
            ";

            migrationBuilder.Sql(sql);


            sql = @"
				Create Function [dbo].[LongitudePlusDistance]
				    (@StartLongitude Float,
				    @StartLatitude Float,
				    @Distance Float)
				Returns Float
				AS
				Begin
				    Return (Select @StartLongitude + Sqrt(@Distance * @Distance / (4784.39411916406 * Cos(2 * @StartLatitude / 114.591559026165) * Cos(2 * @StartLatitude / 114.591559026165))))
				End
			";
	            
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
	        var sql = "DROP Function [dbo].[CalculateDistance]";
	        migrationBuilder.Sql(sql);
	        
	        sql = "DROP Function [dbo].[LongitudePlusDistance]";
	        migrationBuilder.Sql(sql);
        }
    }
}
