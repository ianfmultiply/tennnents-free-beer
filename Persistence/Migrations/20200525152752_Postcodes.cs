﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class Postcodes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Postcodes",
                columns: table => new
                {
                    Name = table.Column<string>(maxLength: 8, nullable: false),
                    Longitude = table.Column<float>(nullable: false),
                    Latitude = table.Column<float>(nullable: false),
                    Country = table.Column<string>(maxLength: 20, nullable: true),
                    Size = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Postcodes", x => x.Name);
                });

            migrationBuilder.InsertData(
                table: "Postcodes",
                columns: new[] { "Name", "Country", "Latitude", "Longitude", "Size" },
                values: new object[,]
                {
                    { "AB10AA", "Scotland", -2.242851f, 5.7101474f, "small" },
                    { "AB10AB", "Scotland", -2.246308f, 57.102554f, "small" },
                    { "AB10AD", "Scotland", -2.248342f, 57.100555f, "small" },
                    { "AB10AE", "Scotland", -2.255708f, 57.084442f, "small" },
                    { "AB10AF", "Scotland", -2.258102f, 57.096657f, "small" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Postcodes");
        }
    }
}
