﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;
using Persistence.SeedData;

namespace Persistence
{
    public class SeedPubs
    {
        public static async Task SeedData(DataContext context)
        {
            // Replace this with pub import
            if (!context.Pubs.Any())
            {
                var pubJson = PubResources.Pubs;
                var pubs = JsonSerializer.Deserialize<List<Pub>>(pubJson);

                foreach (var pub in pubs)
                {
                    pub.Inventory = 80;

                    var coordinates = await context.Postcodes.FirstOrDefaultAsync(x => x.Name == pub.Postcode);
                    pub.Longitude = coordinates.Longitude;
                    pub.Latitude = coordinates.Latitude;
                }

                await context.Pubs.AddRangeAsync(pubs);
            }

            await context.SaveChangesAsync();
        }
    }
}